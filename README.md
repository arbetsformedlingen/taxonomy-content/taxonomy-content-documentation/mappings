## Files

[AF-SCB-diff.xlsx](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/mappings/-/blob/main/AF-SCB_diff.xlsx): Mappings between SSYK groups that differ between the SCB-SSYK and AF-SSYK. Groups that are not mentioned are identical in the two structures. 
